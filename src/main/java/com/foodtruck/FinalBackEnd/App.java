package com.foodtruck.FinalBackEnd;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.foodtruck.FinalBackEnd.Entity.Actualites;
import com.foodtruck.FinalBackEnd.Entity.Adresse_Facturation;
import com.foodtruck.FinalBackEnd.Entity.Adresse_Livraison;
import com.foodtruck.FinalBackEnd.Entity.Commande;
import com.foodtruck.FinalBackEnd.Entity.Commentaire;
import com.foodtruck.FinalBackEnd.Entity.Famille_Repas;
import com.foodtruck.FinalBackEnd.Entity.Genre;
import com.foodtruck.FinalBackEnd.Entity.Ligne_Commande;
import com.foodtruck.FinalBackEnd.Entity.Produit;
import com.foodtruck.FinalBackEnd.Entity.Profil;
import com.foodtruck.FinalBackEnd.Entity.Repas;
import com.foodtruck.FinalBackEnd.Entity.Societe;
import com.foodtruck.FinalBackEnd.Entity.Statut_Commande;
import com.foodtruck.FinalBackEnd.Entity.Utilisateur;
import com.foodtruck.FinalBackEnd.utils.HibernateUtils;


/**
 * Hello world!
 *
 */
public class App 
{
	private static Session s = null;
	private static Transaction tx = null;
	
    public static void main( String[] args )
    {
    	s = HibernateUtils.getSession();
    	tx = s.beginTransaction();
    	///////////////////////////////////////////////////////
    	// Les actualités
    	///////////////////////////////////////////////////////
    	Actualites _news1 = new Actualites("Inauguration de l ouverture notre Food Truck","Venez fêter avec nous la grande ouverture de notre food-truck ce samedi 01 décembre 2018.", "https://www.nrn.com/sites/nrn.com/files/styles/article_featured_retina/public/zaxbys-food-truck-1.gif?itok=xNt8SR_e", true);
    	Actualites _news2 = new Actualites("Promotion exceptionnelle pour les 100 premières commandes","Nous réalisons une ristourne de 25% pour les 100 premières commandes", "http://res.cloudinary.com/hksqkdlah/image/upload/w_1200,h_630,c_fill/ATK%20Food%20Truck/CAN_ATKFoodTruck_5391_23x9", true);
    	Actualites _news3 = new Actualites("La soupe du jour","Cette semaine, nous vous proposons un velouté de potimarrons, servis avec ses croutons à l ail", "http://www.seblartisanculinaire.com/wp-content/uploads/2017/11/seb-camion-food-truck.png", true);
    	
    	
    	s.persist(_news1);
    	s.persist(_news2);
    	s.persist(_news3);
    	
		///////////////////////////////////////////////////////
		// Les commandes 
		///////////////////////////////////////////////////////
    	Statut_Commande _stc1 = new Statut_Commande("En cours de validation");
    	Statut_Commande _stc2 = new Statut_Commande("En cours de préparation");
    	Statut_Commande _stc3 = new Statut_Commande("En cours de livraison");
    	Statut_Commande _stc4 = new Statut_Commande("Commande livrée");
    	Statut_Commande _stc5 = new Statut_Commande("Commande annulée");
    	s.persist(_stc1);
    	s.persist(_stc2);
    	s.persist(_stc3);
    	s.persist(_stc4);
    	s.persist(_stc5);
    	
		///////////////////////////////////////////////////////
		// Les sociétés 
		///////////////////////////////////////////////////////
    	Societe _s1 = new Societe("CAPGEMINI", 5, "rue Frederic Clavel", "92150", "Suresnes","France","47976684200286","479766842");
    	Societe _s2 = new Societe("OnePoint", 22, "Champs de Mars", "45000", "Marseille", "France", "122333009", "23345678987654567654");
    	s.persist(_s1);
    	s.persist(_s2);
    	
		///////////////////////////////////////////////////////
		// Les genres 
		///////////////////////////////////////////////////////
    	Genre 	_g1 = new Genre("Mr");
    	Genre 	_g2 = new Genre("Mlle");
    	Genre 	_g3 = new Genre("Mme");
    	Genre 	_g4 = new Genre("Autre");
    	s.persist(_g1);
    	s.persist(_g2);
    	s.persist(_g3);
    	s.persist(_g4);
    	
    	
		///////////////////////////////////////////////////////
		// Les profils 
		///////////////////////////////////////////////////////
    	Profil 	_pf1 = new Profil("Administrateur");
    	Profil 	_pf2 = new Profil("Commercial");
    	Profil 	_pf3 = new Profil("Gérant");
    	Profil 	_pf4 = new Profil("Client");
    	s.persist(_pf1);
    	s.persist(_pf2);
    	s.persist(_pf3);
    	s.persist(_pf4);
    	
		///////////////////////////////////////////////////////
		// Les adresses de livraison 
		///////////////////////////////////////////////////////
    	
    	_pf4 = (Profil) s.createNamedQuery("Profil.findById", Profil.class)
			.setParameter("myID", 4).uniqueResult();
    	
    	_g1 = (Genre) s.createNamedQuery("Genre.findById", Genre.class)
    			.setParameter("myID", 1).uniqueResult();
    	
    	Adresse_Livraison _adl1 = new Adresse_Livraison(8,"rue Edouard","78415","Flins","France",true);
    	Adresse_Livraison _adl2 = new Adresse_Livraison(7,"rue Jean Baptiste Charcot","91300","Massy","France", true);
    	Adresse_Livraison _adl3 = new Adresse_Livraison(56,"Place du marché","59000","Lille","France",true);
    	Adresse_Livraison _adl4 = new Adresse_Livraison(14,"Rue des Sources","78410","Aubergenville","France", true);
    	Adresse_Livraison _adl5 = new Adresse_Livraison(19,"Rue Rougemont","75009","Paris","France", true);
    	ArrayList<Adresse_Livraison> _listAdresse = new ArrayList<Adresse_Livraison>();
    	_listAdresse.add(_adl1);
    	_listAdresse.add(_adl2);
    	_listAdresse.add(_adl3);
    	_listAdresse.add(_adl5);
    	
		    	
		///////////////////////////////////////////////////////
		// Les adresses de facturation 
		///////////////////////////////////////////////////////
    	Adresse_Facturation _adf1 = new Adresse_Facturation(8,"rue Edouard","78415","Flins","France",true);
    	Adresse_Facturation _adf2 = new Adresse_Facturation(7,"rue Jean Baptiste Charcot","91300","Massy","France", true);
    	Adresse_Facturation _adf3 = new Adresse_Facturation(56,"Place du marché","59000","Lille","France",true);
    	Adresse_Facturation _adf4 = new Adresse_Facturation(14,"Rue des Sources","78410","Aubergenville","France", true);
    	Adresse_Facturation _adf5 = new Adresse_Facturation(19,"Rue Rougemont","75009","Paris","France", true);
    	ArrayList<Adresse_Facturation> _listAdresseF = new ArrayList<Adresse_Facturation>();
    	_listAdresseF.add(_adf1);
    	_listAdresseF.add(_adf2);
    	_listAdresseF.add(_adf3);
    	_listAdresseF.add(_adf5);
    	
		    	
		///////////////////////////////////////////////////////
		// Les utilisateurs 
		///////////////////////////////////////////////////////
    	Utilisateur _u1 = new Utilisateur("AVODAGBE", "Godwin", "gavodagbe", "POO13332:deeefe8!", "hello@ekoura.com", Calendar.getInstance(), true);
    	Utilisateur _u2 = new Utilisateur("Ben Salah", "Raouia", "RBenSalah", "RBenSalah", "zribi-raouia@hotmail.fr", Calendar.getInstance(), true );
    	Utilisateur _u3 = new Utilisateur("Dupond","Pierre", "PDupond", "PDupond!", "pierredupond@gmail.com", Calendar.getInstance(), true);
    	Utilisateur _u4 = new Utilisateur("Brindacier", "Fifi", "FBrindacier", "FBrindacier!", "fifibrindacier@yahoo.com", Calendar.getInstance(), true);
    	Utilisateur _u5 = new Utilisateur("Chault", "Artie", "AChault", "AChaulteeefe8!", "artie-chaud@gmail.com", Calendar.getInstance(), true);
    	
    	_adl1.setUser(_u1);
    	_adl2.setUser(_u1);
    	_adl3.setUser(_u1);
    	_adl5.setUser(_u1);
    	
    	_adf1.setUser(_u1);
    	_adf2.setUser(_u1);
    	_adf3.setUser(_u1);
    	_adf5.setUser(_u1);
    	
    	_u1.setSociete(_s1);
    	_u1.setProfil(_pf4);
    	_u1.setGenre(_g1);
    	_u1.setAdresseLivraisonList(_listAdresse);
    	_u1.setAdresseFacturationList(_listAdresseF);

    	ArrayList<Adresse_Livraison> newListLivAdressUser2  = new ArrayList<Adresse_Livraison>();
    	newListLivAdressUser2.add(_adl4);
    	ArrayList<Adresse_Facturation> newListFactAdressUser2  = new ArrayList<Adresse_Facturation>();
    	newListFactAdressUser2.add(_adf4);
    	
    	_adl4.setUser(_u2);
    	_adf4.setUser(_u2);
    	_u2.setSociete(_s2);
    	_u2.setProfil(_pf1);
    	_u2.setGenre(_g2);
    	_u2.setAdresseLivraisonList(newListLivAdressUser2);
    	_u2.setAdresseFacturationList(newListFactAdressUser2);
    	
    	
    	_u3.setGenre(_g1);
    	_u3.setProfil(_pf4);
    	_u3.setSociete(_s2);
    	
    	_u4.setGenre(_g1);
    	_u4.setProfil(_pf3);
    	_u4.setSociete(_s1);
    	
    	_u5.setGenre(_g1);
    	_u5.setProfil(_pf4);
    	_u5.setSociete(_s1);
    	
    	s.persist(_u1);
    	s.persist(_u2);
    	s.persist(_u3);
    	s.persist(_u4);
    	s.persist(_u5);
    	
    	//s.persist(_adl4);
    	//s.persist(_adf4);
    	// Refresh before for remove last item added to user1
    	//s.refresh(_u1);
    	//s.remove(_u1);
    	
    	
    	
		///////////////////////////////////////////////////////
		// Les repas et familles de repas
		///////////////////////////////////////////////////////
    	
    	Repas _r1 = new Repas("Petit déjeuner", Calendar.getInstance(), true);
    	Repas _r2 = new Repas("Déjeuner", Calendar.getInstance(), true);
    	Repas _r3 = new Repas("Goûter", Calendar.getInstance(), true);
    	Repas _r4 = new Repas("Dîner", Calendar.getInstance(), true);
    	
    	Famille_Repas fr1 = new Famille_Repas("Boissons froides", true);
    	Famille_Repas fr2 = new Famille_Repas("Boissons chaudes", true);
    	Famille_Repas fr3 = new Famille_Repas("Viennoiserie", true);
    	Famille_Repas fr4 = new Famille_Repas("Pain", true);
    	Famille_Repas fr5 = new Famille_Repas("Entrée", true);
    	Famille_Repas fr6 = new Famille_Repas("Plat principal", true);
    	Famille_Repas fr7 = new Famille_Repas("Dessert", true);
    	Famille_Repas fr8 = new Famille_Repas("Hambergeurs", true);
    	Famille_Repas fr9 = new Famille_Repas("Sans gluten", true);
    	
    	ArrayList<Famille_Repas> _listFamilleRepas1 = new ArrayList<Famille_Repas>();
    	_listFamilleRepas1.add(fr1);
    	_listFamilleRepas1.add(fr2);
    	_listFamilleRepas1.add(fr3);
    	_listFamilleRepas1.add(fr4);
    	_r1.setFamille_repas(_listFamilleRepas1);
    	
    	ArrayList<Famille_Repas> _listFamilleRepas2 = new ArrayList<Famille_Repas>();
    	_listFamilleRepas2.add(fr1);
    	_listFamilleRepas2.add(fr2);
    	_listFamilleRepas2.add(fr4);
    	_listFamilleRepas2.add(fr5);
    	_listFamilleRepas2.add(fr6);
    	_listFamilleRepas2.add(fr7);
    	_listFamilleRepas2.add(fr8);
    	_listFamilleRepas2.add(fr9);
    	_r2.setFamille_repas(_listFamilleRepas2);

    	ArrayList<Famille_Repas> _listFamilleRepas3 = new ArrayList<Famille_Repas>();
    	_listFamilleRepas3.add(fr1);
    	_listFamilleRepas3.add(fr2);
    	_listFamilleRepas3.add(fr3);
    	_listFamilleRepas3.add(fr4);
    	_listFamilleRepas3.add(fr7);
    	_listFamilleRepas3.add(fr9);
    	_r3.setFamille_repas(_listFamilleRepas3);

    	ArrayList<Famille_Repas> _listFamilleRepas4 = new ArrayList<Famille_Repas>();
    	_listFamilleRepas4.add(fr1);
    	_listFamilleRepas4.add(fr2);
    	_listFamilleRepas4.add(fr4);
    	_listFamilleRepas4.add(fr5);
    	_listFamilleRepas4.add(fr6);
    	_listFamilleRepas4.add(fr7);
    	_r4.setFamille_repas(_listFamilleRepas4);
    	
    	ArrayList<Repas> _listRepas1 = new ArrayList<Repas>();
    	_listRepas1.add(_r1);
    	_listRepas1.add(_r2);
    	_listRepas1.add(_r3);
    	_listRepas1.add(_r4);
    	fr1.setRepas(_listRepas1);

    	ArrayList<Repas> _listRepas2 = new ArrayList<Repas>();
    	_listRepas2.add(_r1);
    	_listRepas2.add(_r2);
    	_listRepas2.add(_r3);
    	_listRepas2.add(_r4);
    	fr2.setRepas(_listRepas2);
    	
    	ArrayList<Repas> _listRepas3 = new ArrayList<Repas>();
    	_listRepas3.add(_r1);
    	_listRepas3.add(_r3);
    	fr3.setRepas(_listRepas3);
    	
    	ArrayList<Repas> _listRepas4 = new ArrayList<Repas>();
    	_listRepas4.add(_r1);
    	_listRepas4.add(_r2);
    	_listRepas4.add(_r3);
    	_listRepas4.add(_r4);
    	fr4.setRepas(_listRepas4);
    	
    	ArrayList<Repas> _listRepas5 = new ArrayList<Repas>();
    	_listRepas5.add(_r2);
    	_listRepas5.add(_r4);
    	fr5.setRepas(_listRepas5);
    	
    	ArrayList<Repas> _listRepas6 = new ArrayList<Repas>();
    	_listRepas6.add(_r2);
    	_listRepas6.add(_r4);
    	fr6.setRepas(_listRepas6);
    	
    	ArrayList<Repas> _listRepas7 = new ArrayList<Repas>();
    	_listRepas7.add(_r2);
    	_listRepas7.add(_r3);
    	_listRepas7.add(_r4);
    	fr7.setRepas(_listRepas7);
    	
    	
		///////////////////////////////////////////////////////
		// Les produits 
		///////////////////////////////////////////////////////
    	Produit _p1 = new Produit("Eau plate 1/2l", "Eau plate 1/2l", "Eau plate 1/2l", 250, 4, "http://www.hellopro.fr/images/produit-2/3/3/8/24-bouteilles-d-eaux-nestle-aquarel-50-cl-1915833.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p2 = new Produit("Eau plate 1l", "Eau plate 1l", "Eau plate 1l", 300, 2.1, "https://www.welcomeoffice.com/WO_Products_Images/Large/569808.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p3 = new Produit("Eau gazeuse 1/2l", "Eau gazeuse 1/2l", "Eau gazeuse 1/2l", 250, 5.12, "https://i2.cdscdn.com/pdt2/9/6/5/1/300x300/hig5054186625965/rw/highland-spring-encore-2l-eau-de-source-pack-de-6.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p4 = new Produit("Ice Tea Pêche 33cl", "Ice Tea Pêche 33cl", "ice tea frais", 150, 1.5, "https://images-na.ssl-images-amazon.com/images/I/51fCDWlEF8L.jpg", "Lundi, Vendredi", true);
    	Produit _p5 = new Produit("Coca Cola 33cl", "Coca Cola 33cl", "coca cola fraiche", 150, 1.5, "https://aldente-pizza.fr/wp-content/uploads/2018/09/coca-cola-33cl.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p6 = new Produit("thé vert citron", "thé vert citron", "Infusion au citron", 150, 1.99, "https://vitaality.fr/wp-content/uploads/2016/01/©-sommai-Fotolia.com_.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p7 = new Produit("café liegeois", "café liegeois", "café liegeois", 100, 2.45, "http://www.la-cuisine-de-mes-racines.com/wp-content/uploads/2014/03/Café-liégeois.png", "Lundi, Mardi, Vendredi", true);
    	Produit _p8 = new Produit("thé noir", "thé noir", "thé noir", 230, 1.99, "https://ileauxepices.com/blog/wp-content/uploads/2013/05/thé-noir.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p9 = new Produit("Croissant au beurre", "Croissant au beurre", "Croissant au beurre", 100, 1.30, "https://www.ptitchef.com/imgupl/recipe/croissant-au-beurre-en-moins-de-10-minutes-pate--46561p56914.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p10 = new Produit("Croissant aux amandes", "Croissant aux amandes", "Croissant aux amandes", 100, 1.20, "https://i.ytimg.com/vi/GEW_joMhM3Y/maxresdefault.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p11 = new Produit("Torsade au raisin", "Torsade au raisin", "Torsade au raisin", 100, 1.50, "http://www.lalosparis.com/wp-content/uploads/2016/10/Torsade.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p12 = new Produit("Pain multicéréales", "Pain multicéréales", "Pain multicéréales", 100, 1.10, "http://www.marieblachere.com/wp-content/uploads/2013/10/cmapagne-multicéréales-350x250.png", "Lundi, Mardi, Vendredi", true);
    	Produit _p13 = new Produit("Pain complet", "Pain complet", "Pain complet", 100, 1.20, "https://previews.123rf.com/images/mayerkleinostheim/mayerkleinostheim1203/mayerkleinostheim120300778/12795491-le-pain-complet-avec-épi-de-blé.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p14 = new Produit("Pain traditionnel", "Pain traditionnel", "pain traditionnel", 100, 1.35, "https://1.bp.blogspot.com/-xbh7o4j02XQ/U6ZI8qRKj7I/AAAAAAAAQcA/Wb4NI5qa3zQ/s1600/IMG_2599.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p15 = new Produit("Planche mixte", "Planche mixte", "Planche mixte", 100, 8.19, "https://media-cdn.tripadvisor.com/media/photo-s/0d/71/fd/fd/la-petite-planche-mixte.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p16 = new Produit("Soupe du jour", "Soupe du jour", "Soupe du jour", 100, 3.30, "https://www.5amtag.ch/wp-content/uploads/2016/08/exotische_karottesuppe.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p17 = new Produit("Salade césar", "Salade césar", "Salade césar", 100, 5.40, "https://img-3.journaldesfemmes.fr/jkqlaHD119wHCUpvp1OIw_Yk7NM=/750x/smart/image-icu/324872_1254930746.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p18 = new Produit("Tartiflette", "Tartiflette", "Spécialité savoyarde composée de pommes de terre, lardons, reblochon et oignon, et accompagnée d’une bonne salade verte", 100, 4.44, "https://cdn-elle.ladmedia.fr/var/plain_site/storage/images/elle-a-table/recettes-de-cuisine/tartiflette-2072098/21837752-2-fre-FR/Tartiflette.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p19 = new Produit("Coq au vin", "Coq au vin", "','Plat traditionnel de la cuisine française, le coq au vin est accompagné d’oignons, de carottes, d’un bouquet garni et de champignons", 100, 6.79, "https://static.cuisineaz.com/400x320/i130913-coq-au-vin-au-cookeo.jpeg", "Lundi, Mardi, Vendredi", true);
    	Produit _p20 = new Produit("Filet de porc à la moutarde", "Filet de porc à la moutarde", "Filet de porc dans sa sauce à la moutarde, accompagné de pomme de terre et haricots verts", 100, 5.30, "https://www.picard.fr/on/demandware.static/-/Sites-catalog-picard/default/dw89abe746/produits/viandes-volailles/edition/000000000000015756_E1.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p21 = new Produit("Tarte tatin", "tarte tatin", "tarte faite maison", 100, 3.5, "https://files.meilleurduchef.com/mdc/photo/recette/tarte-tatin/tarte-tatin-640.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p22 = new Produit("Mousse au chocolat", "Mousse au chocolat", "mélange de chocolat noir et chocolat blanc", 500, 1.30, "https://files.meilleurduchef.com/mdc/photo/recette/mousse-chocolat/mousse-chocolat-640.jpg", "Lundi, Mardi, Vendredi", true);
    	Produit _p23 = new Produit("Fruits de saison", "Fruits de saison", "Fruits frais produits en France", 100, 1.90, "http://idata.over-blog.com/5/03/61/53/revue-de-presse-8/1-fruits.jpg", "Lundi, Mardi, Vendredi", true);
    	
    	_p1.setFamille_repas(fr1);
    	_p2.setFamille_repas(fr1);
    	_p3.setFamille_repas(fr1);
    	_p4.setFamille_repas(fr1);
    	_p5.setFamille_repas(fr1);
    	_p6.setFamille_repas(fr2);
    	_p7.setFamille_repas(fr2);
    	_p8.setFamille_repas(fr2);
    	_p9.setFamille_repas(fr3);
    	_p10.setFamille_repas(fr3);
    	_p11.setFamille_repas(fr3);
    	_p12.setFamille_repas(fr4);
    	_p13.setFamille_repas(fr4);
    	_p14.setFamille_repas(fr4);
    	_p15.setFamille_repas(fr5);
    	_p16.setFamille_repas(fr5);
    	_p17.setFamille_repas(fr5);
    	_p18.setFamille_repas(fr6);
    	_p19.setFamille_repas(fr6);
    	_p20.setFamille_repas(fr6);
    	_p21.setFamille_repas(fr7);
    	_p22.setFamille_repas(fr7);
    	_p23.setFamille_repas(fr7);
    	
    	
    	
    	
    	
    	s.persist(_r1);
    	s.persist(_r2);
    	s.persist(_r3);
    	s.persist(_r4);
    	
    	s.persist(_p1);
    	s.persist(_p2);
    	s.persist(_p3);
    	s.persist(_p4);
    	s.persist(_p5);
    	s.persist(_p6);
    	s.persist(_p7);
    	s.persist(_p8);
    	s.persist(_p9);
    	s.persist(_p10);
    	s.persist(_p11);
    	s.persist(_p12);
    	s.persist(_p13);
    	s.persist(_p14);
    	s.persist(_p15);
    	s.persist(_p16);
    	s.persist(_p17);
    	s.persist(_p18);
    	s.persist(_p19);
    	s.persist(_p20);
    	s.persist(_p21);
    	s.persist(_p22);
    	s.persist(_p23);
    	
		///////////////////////////////////////////////////////
		// Les commandes
		///////////////////////////////////////////////////////
    	Commande _c1 = new Commande(10.99, new GregorianCalendar(2018, 10, 1), "79KGR237");
    	
    	Commande _c2 = new Commande(10.99, new GregorianCalendar(2018, 10, 12), "05HBD986");
    	
    	Commande _c3 = new Commande(10.99, new GregorianCalendar(2018, 11, 10), "93DTB345");
    	
    	
    	_c1.setAdresse_facturation(_u1.getAdresseFacturationList().get(0));
    	_c1.setAdresse_livraison(_u1.getAdresseLivraisonList().get(0));
    	_c1.setStatut_commande(_stc4);
    	
    	_c2.setAdresse_facturation(_u1.getAdresseFacturationList().get(1));
    	_c2.setAdresse_livraison(_u1.getAdresseLivraisonList().get(1));
    	_c2.setStatut_commande(_stc4);
    	
    	_c3.setAdresse_facturation(_u1.getAdresseFacturationList().get(2));
    	_c3.setAdresse_livraison(_u1.getAdresseLivraisonList().get(2));
    	_c3.setStatut_commande(_stc3);
    	
    	_c1.setUserCommande(_u1);
    	_c2.setUserCommande(_u1);
    	_c3.setUserCommande(_u1);
    	
    	s.persist(_c1);
    	s.persist(_c2);
    	s.persist(_c3);
    	
    	///////////////////////////////////////////////////////
		// Les commandes
		///////////////////////////////////////////////////////
    	Ligne_Commande _lc1 = new Ligne_Commande(2, 4);
    	Ligne_Commande _lc2 = new Ligne_Commande(1,2);
    	Ligne_Commande _lc3 = new Ligne_Commande(1,5);
    	Ligne_Commande _lc4 = new Ligne_Commande(1,8);
    	Ligne_Commande _lc5 = new Ligne_Commande(1,15.5);
    	Ligne_Commande _lc6 = new Ligne_Commande(1,4);
    	Ligne_Commande _lc7 = new Ligne_Commande(3,1.5);
    	Ligne_Commande _lc8 = new Ligne_Commande(1,1);
    	Ligne_Commande _lc9 = new Ligne_Commande(2, 4);
    	
    	_lc1.setCommande(_c1);
    	_lc1.setProduit(_p1);

    	_lc2.setCommande(_c1);
    	_lc2.setProduit(_p15);

    	_lc3.setCommande(_c1);
    	_lc3.setProduit(_p13);

    	_lc4.setCommande(_c2);
    	_lc4.setProduit(_p22);
    	
    	_lc5.setCommande(_c2);
    	_lc5.setProduit(_p23);
    	
    	_lc6.setCommande(_c2);
    	_lc6.setProduit(_p19);
    	
    	_lc7.setCommande(_c3);
    	_lc7.setProduit(_p6);

    	_lc8.setCommande(_c3);
    	_lc8.setProduit(_p18);

    	_lc9.setCommande(_c3);
    	_lc9.setProduit(_p17);
    	
    	
    	s.persist(_lc1);
    	s.persist(_lc2);
    	s.persist(_lc3);
    	s.persist(_lc4);
    	s.persist(_lc5);
    	s.persist(_lc6);
    	s.persist(_lc7);
    	s.persist(_lc8);
    	s.persist(_lc9);

		    	
		///////////////////////////////////////////////////////
		// Les commentaires
		///////////////////////////////////////////////////////
    	Commentaire _com1 = new Commentaire("Génial, je me suis régalé!", 5	, Calendar.getInstance());
    	_com1.setLigneCommande(_lc1);
    	Commentaire _com2 = new Commentaire("Génial, je me suis régalé!", 5	, Calendar.getInstance());
    	_com2.setLigneCommande(_lc8);
    	Commentaire _com3 = new Commentaire("Génial, je me suis régalé!", 5	, Calendar.getInstance());
    	_com3.setLigneCommande(_lc5);
    	Commentaire _com4 = new Commentaire("Génial, je me suis régalé!", 5	, Calendar.getInstance());
    	_com4.setLigneCommande(_lc9);
    	
    	//s.persist(_com1);
    	//s.persist(_com2);
    	//s.persist(_com3);
    	//s.persist(_com4);
    	
    	tx.commit();
    	s.close();
    	
    }
}
